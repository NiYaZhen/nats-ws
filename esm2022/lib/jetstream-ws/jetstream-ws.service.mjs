import { Injectable } from '@angular/core';
import { StringCodec, connect, } from 'nats.ws';
import * as i0 from "@angular/core";
export class JetStreamWsService {
    #nc;
    #jsm;
    #js;
    // 創建與 webSocket的連接 ws://0.0.0.0:8080
    async createConnection() {
        try {
            this.#nc = await connect(this.option.connectionOptions);
            console.log(`connected to ${this.#nc.getServer()}`);
        }
        catch (error) {
            console.log(`連線失敗`);
        }
        await this.#setStreamConfig();
    }
    async #setStreamConfig() {
        this.#jsm = await this.#nc.jetstreamManager();
        const { streamConfig } = this.option;
        const streams = await this.#jsm.streams.list().next();
        const stream = streams.find((stream) => stream.config.name === streamConfig?.name);
        if (stream) {
            const streamSubject = new Set([
                ...stream.config.subjects,
                ...streamConfig.subjects,
            ]);
            const streamInfo = await this.#jsm.streams.update(stream.config.name, {
                ...stream.config,
                ...streamConfig,
                subjects: [...streamSubject.keys()],
            });
            console.log(`Stream ${streamInfo.config.name} updated`);
        }
        else {
            this.#createStream();
        }
    }
    async #createStream() {
        const { streamConfig } = this.option;
        const streamInfo = await this.#jsm.streams.add(streamConfig);
        console.log(`Stream ${streamInfo.config.name} created`);
    }
    // create PullConsumer
    async createConsumer(stream) {
        try {
            this.#jsm = await this.#nc.jetstreamManager();
            const consumerInfo = this.#jsm.consumers.add(stream, this.option.consumerOptions);
            console.log(`create consumer ${(await consumerInfo).name} success`);
        }
        catch (err) {
            console.log(`創建失敗`);
        }
    }
    // 發送事件訊息
    async sendEventMessage(subject, event) {
        try {
            this.#js = this.#nc.jetstream();
            const sc = StringCodec();
            await this.#js.publish(subject, sc.encode(event));
            console.log(`發送成功`);
        }
        catch (err) {
            console.log(`發送失敗`);
        }
    }
    // 接收事件訊息
    async receiveEventMessage(stream, consumer) {
        this.#js = this.#nc.jetstream();
        const consumrApi = await this.#js.consumers.get(stream, consumer);
        const messages = await consumrApi.consume();
        try {
            for await (const message of messages) {
                console.log(message.seq);
                console.log(`consumer fetch: ${message.subject}`);
            }
        }
        catch (err) {
            console.log(`consume failed`);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiamV0c3RyZWFtLXdzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9qZXRzdHJlYW0td3Mvc3JjL2xpYi9qZXRzdHJlYW0td3MvamV0c3RyZWFtLXdzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBSUwsV0FBVyxFQUNYLE9BQU8sR0FDUixNQUFNLFNBQVMsQ0FBQzs7QUFNakIsTUFBTSxPQUFPLGtCQUFrQjtJQUc3QixHQUFHLENBQWtCO0lBQ3JCLElBQUksQ0FBb0I7SUFDeEIsR0FBRyxDQUFtQjtJQUV0QixxQ0FBcUM7SUFDckMsS0FBSyxDQUFDLGdCQUFnQjtRQUNwQixJQUFJO1lBQ0YsSUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDckQ7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDckI7UUFFRCxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxLQUFLLENBQUMsZ0JBQWdCO1FBQ3BCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDOUMsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFFckMsTUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUV0RCxNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUN6QixDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFLElBQUksQ0FDdEQsQ0FBQztRQUVGLElBQUksTUFBTSxFQUFFO1lBQ1YsTUFBTSxhQUFhLEdBQUcsSUFBSSxHQUFHLENBQUM7Z0JBQzVCLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRO2dCQUN6QixHQUFHLFlBQWEsQ0FBQyxRQUFRO2FBQzFCLENBQUMsQ0FBQztZQUVILE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUNwRSxHQUFHLE1BQU0sQ0FBQyxNQUFNO2dCQUNoQixHQUFHLFlBQVk7Z0JBQ2YsUUFBUSxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDcEMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQztTQUN6RDthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQUVELEtBQUssQ0FBQyxhQUFhO1FBQ2pCLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JDLE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQWEsQ0FBQyxDQUFDO1FBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVELHNCQUFzQjtJQUN0QixLQUFLLENBQUMsY0FBYyxDQUFDLE1BQWM7UUFDakMsSUFBSTtZQUNGLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDOUMsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUMxQyxNQUFNLEVBQ04sSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQzVCLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sWUFBWSxDQUFDLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQztTQUNyRTtRQUFDLE9BQU8sR0FBRyxFQUFFO1lBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNyQjtJQUNILENBQUM7SUFDRCxTQUFTO0lBQ1QsS0FBSyxDQUFDLGdCQUFnQixDQUFDLE9BQWUsRUFBRSxLQUFhO1FBQ25ELElBQUk7WUFDRixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDaEMsTUFBTSxFQUFFLEdBQUcsV0FBVyxFQUFFLENBQUM7WUFDekIsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBRWxELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDckI7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBQ0QsU0FBUztJQUNULEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxNQUFjLEVBQUUsUUFBZ0I7UUFDeEQsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hDLE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNsRSxNQUFNLFFBQVEsR0FBRyxNQUFNLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUU1QyxJQUFJO1lBQ0YsSUFBSSxLQUFLLEVBQUUsTUFBTSxPQUFPLElBQUksUUFBUSxFQUFFO2dCQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7YUFDbkQ7U0FDRjtRQUFDLE9BQU8sR0FBRyxFQUFFO1lBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs4R0E1RlUsa0JBQWtCO2tIQUFsQixrQkFBa0IsY0FGakIsTUFBTTs7MkZBRVAsa0JBQWtCO2tCQUg5QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHtcbiAgSmV0U3RyZWFtQ2xpZW50LFxuICBKZXRTdHJlYW1NYW5hZ2VyLFxuICBOYXRzQ29ubmVjdGlvbixcbiAgU3RyaW5nQ29kZWMsXG4gIGNvbm5lY3QsXG59IGZyb20gJ25hdHMud3MnO1xuaW1wb3J0IHsgSmV0c3RyZWFtV3NPcHRpb24gfSBmcm9tICcuL2pldHN0cmVhbS13cy5pbnRlcmZhY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290Jyxcbn0pXG5leHBvcnQgY2xhc3MgSmV0U3RyZWFtV3NTZXJ2aWNlIHtcbiAgb3B0aW9uITogSmV0c3RyZWFtV3NPcHRpb247XG5cbiAgI25jITogTmF0c0Nvbm5lY3Rpb247XG4gICNqc20hOiBKZXRTdHJlYW1NYW5hZ2VyO1xuICAjanMhOiBKZXRTdHJlYW1DbGllbnQ7XG5cbiAgLy8g5Ym15bu66IiHIHdlYlNvY2tldOeahOmAo+aOpSB3czovLzAuMC4wLjA6ODA4MFxuICBhc3luYyBjcmVhdGVDb25uZWN0aW9uKCkge1xuICAgIHRyeSB7XG4gICAgICB0aGlzLiNuYyA9IGF3YWl0IGNvbm5lY3QodGhpcy5vcHRpb24uY29ubmVjdGlvbk9wdGlvbnMpO1xuICAgICAgY29uc29sZS5sb2coYGNvbm5lY3RlZCB0byAke3RoaXMuI25jLmdldFNlcnZlcigpfWApO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmxvZyhg6YCj57ea5aSx5pWXYCk7XG4gICAgfVxuXG4gICAgYXdhaXQgdGhpcy4jc2V0U3RyZWFtQ29uZmlnKCk7XG4gIH1cblxuICBhc3luYyAjc2V0U3RyZWFtQ29uZmlnKCkge1xuICAgIHRoaXMuI2pzbSA9IGF3YWl0IHRoaXMuI25jLmpldHN0cmVhbU1hbmFnZXIoKTtcbiAgICBjb25zdCB7IHN0cmVhbUNvbmZpZyB9ID0gdGhpcy5vcHRpb247XG5cbiAgICBjb25zdCBzdHJlYW1zID0gYXdhaXQgdGhpcy4janNtLnN0cmVhbXMubGlzdCgpLm5leHQoKTtcblxuICAgIGNvbnN0IHN0cmVhbSA9IHN0cmVhbXMuZmluZChcbiAgICAgIChzdHJlYW0pID0+IHN0cmVhbS5jb25maWcubmFtZSA9PT0gc3RyZWFtQ29uZmlnPy5uYW1lLFxuICAgICk7XG5cbiAgICBpZiAoc3RyZWFtKSB7XG4gICAgICBjb25zdCBzdHJlYW1TdWJqZWN0ID0gbmV3IFNldChbXG4gICAgICAgIC4uLnN0cmVhbS5jb25maWcuc3ViamVjdHMsXG4gICAgICAgIC4uLnN0cmVhbUNvbmZpZyEuc3ViamVjdHMsXG4gICAgICBdKTtcblxuICAgICAgY29uc3Qgc3RyZWFtSW5mbyA9IGF3YWl0IHRoaXMuI2pzbS5zdHJlYW1zLnVwZGF0ZShzdHJlYW0uY29uZmlnLm5hbWUsIHtcbiAgICAgICAgLi4uc3RyZWFtLmNvbmZpZyxcbiAgICAgICAgLi4uc3RyZWFtQ29uZmlnLFxuICAgICAgICBzdWJqZWN0czogWy4uLnN0cmVhbVN1YmplY3Qua2V5cygpXSxcbiAgICAgIH0pO1xuXG4gICAgICBjb25zb2xlLmxvZyhgU3RyZWFtICR7c3RyZWFtSW5mby5jb25maWcubmFtZX0gdXBkYXRlZGApO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLiNjcmVhdGVTdHJlYW0oKTtcbiAgICB9XG4gIH1cblxuICBhc3luYyAjY3JlYXRlU3RyZWFtKCkge1xuICAgIGNvbnN0IHsgc3RyZWFtQ29uZmlnIH0gPSB0aGlzLm9wdGlvbjtcbiAgICBjb25zdCBzdHJlYW1JbmZvID0gYXdhaXQgdGhpcy4janNtLnN0cmVhbXMuYWRkKHN0cmVhbUNvbmZpZyEpO1xuICAgIGNvbnNvbGUubG9nKGBTdHJlYW0gJHtzdHJlYW1JbmZvLmNvbmZpZy5uYW1lfSBjcmVhdGVkYCk7XG4gIH1cblxuICAvLyBjcmVhdGUgUHVsbENvbnN1bWVyXG4gIGFzeW5jIGNyZWF0ZUNvbnN1bWVyKHN0cmVhbTogc3RyaW5nKSB7XG4gICAgdHJ5IHtcbiAgICAgIHRoaXMuI2pzbSA9IGF3YWl0IHRoaXMuI25jLmpldHN0cmVhbU1hbmFnZXIoKTtcbiAgICAgIGNvbnN0IGNvbnN1bWVySW5mbyA9IHRoaXMuI2pzbS5jb25zdW1lcnMuYWRkKFxuICAgICAgICBzdHJlYW0sXG4gICAgICAgIHRoaXMub3B0aW9uLmNvbnN1bWVyT3B0aW9ucyxcbiAgICAgICk7XG4gICAgICBjb25zb2xlLmxvZyhgY3JlYXRlIGNvbnN1bWVyICR7KGF3YWl0IGNvbnN1bWVySW5mbykubmFtZX0gc3VjY2Vzc2ApO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgY29uc29sZS5sb2coYOWJteW7uuWkseaVl2ApO1xuICAgIH1cbiAgfVxuICAvLyDnmbzpgIHkuovku7boqIrmga9cbiAgYXN5bmMgc2VuZEV2ZW50TWVzc2FnZShzdWJqZWN0OiBzdHJpbmcsIGV2ZW50OiBzdHJpbmcpIHtcbiAgICB0cnkge1xuICAgICAgdGhpcy4janMgPSB0aGlzLiNuYy5qZXRzdHJlYW0oKTtcbiAgICAgIGNvbnN0IHNjID0gU3RyaW5nQ29kZWMoKTtcbiAgICAgIGF3YWl0IHRoaXMuI2pzLnB1Ymxpc2goc3ViamVjdCwgc2MuZW5jb2RlKGV2ZW50KSk7XG5cbiAgICAgIGNvbnNvbGUubG9nKGDnmbzpgIHmiJDlip9gKTtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGNvbnNvbGUubG9nKGDnmbzpgIHlpLHmlZdgKTtcbiAgICB9XG4gIH1cbiAgLy8g5o6l5pS25LqL5Lu26KiK5oGvXG4gIGFzeW5jIHJlY2VpdmVFdmVudE1lc3NhZ2Uoc3RyZWFtOiBzdHJpbmcsIGNvbnN1bWVyOiBzdHJpbmcpIHtcbiAgICB0aGlzLiNqcyA9IHRoaXMuI25jLmpldHN0cmVhbSgpO1xuICAgIGNvbnN0IGNvbnN1bXJBcGkgPSBhd2FpdCB0aGlzLiNqcy5jb25zdW1lcnMuZ2V0KHN0cmVhbSwgY29uc3VtZXIpO1xuICAgIGNvbnN0IG1lc3NhZ2VzID0gYXdhaXQgY29uc3VtckFwaS5jb25zdW1lKCk7XG5cbiAgICB0cnkge1xuICAgICAgZm9yIGF3YWl0IChjb25zdCBtZXNzYWdlIG9mIG1lc3NhZ2VzKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKG1lc3NhZ2Uuc2VxKTtcbiAgICAgICAgY29uc29sZS5sb2coYGNvbnN1bWVyIGZldGNoOiAke21lc3NhZ2Uuc3ViamVjdH1gKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIGNvbnNvbGUubG9nKGBjb25zdW1lIGZhaWxlZGApO1xuICAgIH1cbiAgfVxufVxuIl19