import { JetstreamWsOption } from './jetstream-ws.interface';
import * as i0 from "@angular/core";
export declare class JetStreamWsService {
    #private;
    option: JetstreamWsOption;
    createConnection(): Promise<void>;
    createConsumer(stream: string): Promise<void>;
    sendEventMessage(subject: string, event: string): Promise<void>;
    receiveEventMessage(stream: string, consumer: string): Promise<void>;
    static ɵfac: i0.ɵɵFactoryDeclaration<JetStreamWsService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<JetStreamWsService>;
}
