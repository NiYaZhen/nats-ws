import { ConnectionOptions, ConsumerConfig, JetStreamOptions, NatsConnection } from 'nats.ws';
export interface JetstreamWsOption {
    connectionOptions: Partial<NatsConnectionOptions> & Pick<NatsConnectionOptions, 'name'>;
    consumerOptions: Partial<ConsumerConfig>;
    jetStreamOptions?: JetStreamOptions;
    streamConfig?: NatsStreamConfig;
}
export interface NatsConnectionOptions extends ConnectionOptions {
    name: string;
    connectedHook?: (nc: NatsConnection) => void;
}
import { StreamConfig } from 'nats.ws';
type $StreamConfig = Pick<StreamConfig, 'storage' | 'retention' | 'discard' | 'max_msgs' | 'max_msgs_per_subject' | 'max_msg_size' | 'max_age' | 'duplicate_window' | 'num_replicas'>;
export interface NatsStreamConfig extends Partial<$StreamConfig> {
    name: string;
    subjects: string[];
}
export {};
